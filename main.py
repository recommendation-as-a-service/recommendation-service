"""
This script handle the running the algos on the data sets
It gets the data from S3 from a job in RabbitMQ,
then stores the processed data in S3 and a record in MongoDB
"""
import sys
import time
import json
import pika
import pymongo
import boto3
import botocore
import datetime
import math

from sklearn.cluster import KMeans
from sklearn.neighbors import NearestNeighbors
from sklearn.decomposition import NMF
from sklearn.metrics import pairwise
from sklearn.metrics import mean_squared_error

import scipy.spatial as sp
import numpy as np

from csr import CSR

ENV = sys.argv[1]

DB_CLIENT = pymongo.MongoClient("mongodb://{}".format("data-db" if ENV == "production" else "159.203.64.136:27018"))
RAAS_DB = DB_CLIENT.raas
RECORDS = RAAS_DB.records

def get_s3():
    """
    Gets a bucket from S3
    """
    cred = json.load(open("aws.config.json"))
    return boto3.resource('s3', aws_access_key_id=cred["accessKeyId"],
                          aws_secret_access_key=cred["secretAccessKey"],
                          config=botocore.config.Config("us-east-2"))


def get_file_from_s3(file_name):
    """
    Gets a file from a bucket
    """
    obj = get_s3().Bucket('raas-final-project').Object(file_name)
    buffer = obj.get()["Body"].read()
    return str(buffer)

def put_csv(file_string, name):
    """
    Save a string/csv into S3
    """
    get_s3().Bucket('raas-final-project').Object(name).put(Body=file_string.encode('UTF-8'), ACL='public-read')
    return "https://s3.us-east-2.amazonaws.com/raas-final-project/" + name

def csv_to_matrix(csv):
    csv = csv[2:-1]
    csv_list = []
    for row in csv.split('\\n'):
        csv_list.append(row.split(','))
        if csv_list[-1] == ['']:
            csv_list.pop()
        else:
            if not csv_list[-1][-1].isnumeric():
                csv_list[-1].pop()

            csv_list[-1] = np.array(csv_list[-1])


    return np.array(csv_list)

def method_distributer(algorithm, file_url, user_id, message):
    """
    Handles which algorithm gets ran
    """
    algorithms = {
        "csr" : run_csr,
        "kmeans" : k_means_clustering,
        "cosine" : cosine_similarity,
        # "knn" : k_nearest_neighbor_clustering,
        "matrix-decomposition" : matrix_decomposition
    }
    if algorithm in algorithms.keys():
        start_time = datetime.datetime.now()
        algorithms[algorithm](get_file_from_s3(message["fileName"]), message, start_time)
    else:
        print("Invalid Algorithm: {}".format(algorithm))


def run_csr(csv, message, start_time):
    """
    Writes out the CSR
    """
    csr_string = ""
    values = 0
    matrix = csv_to_matrix(csv)

    for i in matrix:
        for j in range(len(i)):
            if not i[j] == 0:
                values = values + 1
                csr_string = csr_string + "{} {} ".format(j, i[j])
        csr_string = csr_string + '\n'

    csr_string = "{} {} {}".format(len(matrix), len(matrix[0]), values) + '\n\n' + csr_string

    url = put_csv(csr_string, message["fileName"] + '.txt')
    time_difference = (datetime.datetime.now - start_time).total_seconds
    RECORDS.insert_one({
        "url": url,
        "userId": message["userId"],
        "uploadId": message["uploadId"],
        "algorithm": "csr",
        "completion_time": time_difference})

    print("Finished CSR")


def cosine_similarity(csv, message, start_time):
    """
    Finds the Cosine Similarity
    """

    matrix = csv_to_matrix(csv)
    kernel = pairwise.cosine_similarity(matrix.transpose())

    """
    Find error
    """
    prediction = matrix.dot(kernel).round(0)
    error = math.sqrt(mean_squared_error(matrix, prediction))

    """
    Find recommendations for a given dataset
    """
    recommendations = np.empty(matrix.shape)
    for idx, row in enumerate(matrix):
        recommendations[idx] = row.dot(kernel[idx])

    kernel_string = ""

    for i in recommendations:
        for j in i:
            kernel_string = kernel_string + ",{}".format(j)
        kernel_string = kernel_string + '\n'

    url = put_csv(kernel_string, "cosine-{}".format(message["fileName"]))
    time_difference = (datetime.datetime.now - start_time).total_seconds
    RECORDS.insert_one({
        "url": url,
        "userId": message["userId"],
        "uploadId": message["uploadId"],
        "algorithm": "cosine",
        "comppletion_time": time_difference,
        "error": error})

    print("Finished Cosine")

def k_means_clustering(csv, message, start_time):
    """
    runs the kmeans clustering algorithm
    """

    matrix = csv_to_matrix(csv)
    kmeans = KMeans(n_clusters=message["k"], random_state=0).fit(matrix)
    time_difference = (datetime.datetime.now - start_time).total_seconds
    RECORDS.insert_one({
        "userId": message["userId"],
        "uploadId": message["uploadId"],
        "algorithm": "kmeans",
        "centroids": kmeans.cluster_centers_.tolist(),
        "completion_time": time_difference})

    print("Finished Kmeans")

# def k_nearest_neighbor_clustering(csr, message, start_time):
#     """
#     runs KNN algo
#     """
#     f = open(str(csr.user_id) + "_kNNClustering.txt", 'w')
#     nbrs = NearestNeighbors(n_neighbors=11, algorithm='ball_tree').fit(csr.transposed_matrix)
#     distances, indices = nbrs.kneighbors(csr.transposed_matrix)
#     for row in indices:
#         for idx, val in enumerate(row):
#             if idx == 0:
#                 next
#             else:
#                 f.write(str(val) + " ")
#         f.write("\n")

def matrix_decomposition(csv, message, start_time):
    """
    decomposes the matrix into two
    """

    dimensinoality = int(message["k"])
    matrix = csv_to_matrix(csv)
    model = NMF(n_components=dimensionality, init='random', random_state=0, max_iter=2000).fit(matrix)

    U = model.fit_transform(matrix)
    V = model.components_

    prediction = U.dot(V)

    error = math.sqrt(mean_squared_error(matrix, prediction))

    U_string = ""
    V_string = ""

    for i in U:
        for j in i:
            U_string = U_string + "{},".format(j)
        U_string = U_string + '\n'

    for i in V:
        for j in i:
            V_string = V_string + "{},".format(j)
        V_string = V_string + '\n'

    url = put_csv(U_string, "matrix-decomposition-U-{}".format(message["fileName"]))
    matrix_V_url = put_csv(V_string, "matrix-decomposition-V-{}".format(message["fileName"]))
    time_difference = (datetime.datetime.now - start_time).total_seconds
    RECORDS.insert_one({
        "url": url,
        "userId": message["userId"],
        "uploadId": message["uploadId"],
        "algorithm": "matrix-decomposition",
        "matrixVUrl": matrix_V_url,
        "completion_time": time_difference,
        "error": error})

    print("Finished Matrix Decomp")


KEEP_GOING = True
i = 0
while KEEP_GOING and i < 30:
    HOST = "rabbitmq" if ENV == "production" else "159.203.64.136"
    try:
        print("Connection try {}".format(i))
        CONNECTION_PARAMS = pika.ConnectionParameters(host=HOST)
        CONNECTION = pika.BlockingConnection(CONNECTION_PARAMS)
        CHANNEL = CONNECTION.channel()
        KEEP_GOING = False
    except pika.exceptions.ConnectionClosed:
        time.sleep(1)
        i = i + 1

def callback(ch, method, properties, body):
    """
    This function gets ran when something is in the queue
    """
    message = json.loads(str(body)[2:-1])
    print("Running {} Algo".format(message["algorithm"]))
    method_distributer(message["algorithm"], message["uploadId"], message["userId"], message)


CHANNEL.queue_declare(queue='recommender')
CHANNEL.basic_consume(callback, queue='recommender', no_ack=True)
print('Waiting for messages. To exit press CTRL+C')
CHANNEL.start_consuming()
