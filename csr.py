"""
This module handles our CSR representation.
"""
import csv
import numpy as np

class CSR():
    """
    This class represents a CSR
    """
    def __init__(self, input_file, user_id):
        self.file = input_file
        self.values = []
        self.columns = []
        self.row_pointers = []
        self.column_count = 0
        self.row_count = 0
        self.value_count = 0
        self.output_file = str(user_id) + '_csrOutput.txt'
        self.matrix = 0
        self.transposed_matrix = 0
        self.user_id = user_id

    def create_csr(self):
        """
        creates the CSR
        """
        reader = csv.reader(open(self.file), delimiter=' ')
        i = 0
        row_pointer = 0
        for row in reader:
            if i == 0:
                self.row_count = row[0]
                self.column_count = row[1]
                self.value_count = row[2]
                i = i + 1
            else:
                current_row = [x for x in row if x]
                for i, val in enumerate(current_row):
                    if i%2 == 0:
                        self.columns.append(val)
                    else:
                        self.values.append(val)
                        row_pointer = row_pointer + 1
            self.row_pointers.append(row_pointer)

    def output_csr(self):
        """
        Writes the CSR
        """
        f = open(self.output_file, 'w')
        for item in self.columns:
            f.write(str(item) + " ")
        f.write("\n")
        for item in self.values:
            f.write(str(item) + " ")
        f.write("\n")
        for item in self.row_pointers:
            f.write(str(item) + " ")

    def read_in_stored_csr(self):
        """
        Reads in a CSR
        """ 
        reader = csv.reader(open(self.file), delimiter = ' ')
        i = 0
        for row in reader:
            current_row = [x for x in row if x]
            if i == 0:
                self.columns = current_row
                i = i + 1
            elif i == 1:
                self.values = current_row
                i = i + 1
            else:
                self.row_pointers = current_row

    def transpose_csr(self):
        """
        Handles the transpose of a CSR
        """
        s = (int(self.row_count), int(self.column_count))
        self.matrix = np.zeros(s)
        current_row = 1
        current_loc_in_row_pointers = 1
        value_count = 0
        
        for value, column in zip(self.values, self.columns):
            self.matrix[int(current_row) - 1, int(column) - 1] = value
            value_count = value_count + 1
            if value_count == self.row_pointers[current_loc_in_row_pointers] - self.row_pointers[current_loc_in_row_pointers - 1]:
                current_row += 1
                value_count = 0
                current_loc_in_row_pointers += 1
        self.transposed_matrix = self.matrix.transpose()
